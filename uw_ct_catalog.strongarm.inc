<?php
/**
 * @file
 * uw_ct_catalog.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_ct_catalog_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_uw_catalog';
  $strongarm->value = 0;
  $export['comment_anonymous_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_uw_catalog';
  $strongarm->value = 1;
  $export['comment_default_mode_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_uw_catalog';
  $strongarm->value = '50';
  $export['comment_default_per_page_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_uw_catalog';
  $strongarm->value = 0;
  $export['comment_form_location_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_uw_catalog';
  $strongarm->value = '0';
  $export['comment_preview_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_uw_catalog';
  $strongarm->value = 0;
  $export['comment_subject_field_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_uw_catalog';
  $strongarm->value = '1';
  $export['comment_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'default_menu_link_enabled_uw_catalog';
  $strongarm->value = 1;
  $export['default_menu_link_enabled_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_uw_catalog';
  $strongarm->value = 1;
  $export['enable_revisions_page_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__uw_catalog';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'entity_teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'ical' => array(
        'custom_settings' => TRUE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'embedded' => array(
        'custom_settings' => TRUE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '28',
        ),
        'redirect' => array(
          'weight' => '30',
        ),
        'xmlsitemap' => array(
          'weight' => '27',
        ),
        'locations' => array(
          'weight' => '31',
        ),
        'language' => array(
          'weight' => '33',
        ),
        'metatags' => array(
          'weight' => '32',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_uw_catalog';
  $strongarm->value = '4';
  $export['language_content_type_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_uw_catalog';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_uw_catalog';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_uw_catalog';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_uw_ct_catalog';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_uw_ct_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_uw_catalog';
  $strongarm->value = '1';
  $export['node_preview_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_uw_catalog';
  $strongarm->value = 0;
  $export['node_submitted_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_title_type_uw_catalog';
  $strongarm->value = '';
  $export['page_title_type_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_uw_catalog_pattern';
  $strongarm->value = 'catalog/[node:title]';
  $export['pathauto_node_uw_catalog_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_catalog_categories_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_catalog_categories_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_enable_uw_catalog';
  $strongarm->value = 1;
  $export['scheduler_publish_enable_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_revision_uw_catalog';
  $strongarm->value = 1;
  $export['scheduler_publish_revision_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_touch_uw_catalog';
  $strongarm->value = 1;
  $export['scheduler_publish_touch_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_enable_uw_catalog';
  $strongarm->value = 1;
  $export['scheduler_unpublish_enable_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_revision_uw_catalog';
  $strongarm->value = 1;
  $export['scheduler_unpublish_revision_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'searcher_config';
  $strongarm->value = array(
    'forms' => array(
      'toggle_forms' => 0,
      'move_keyword_search' => 0,
      'advanced_populate' => 0,
      'remove_containing_wrapper' => 'default',
      'advanced_expand' => 'default',
    ),
    'fields' => array(
      'containing_any' => array(
        'remove' => 0,
        'roles' => array(
          1 => 0,
          2 => 0,
          16 => 0,
          13 => 0,
          14 => 0,
          15 => 0,
          3 => 0,
          6 => 0,
          12 => 0,
          5 => 0,
          4 => 0,
          7 => 0,
          8 => 0,
          9 => 0,
          11 => 0,
          10 => 0,
          17 => 0,
        ),
      ),
      'containing_phrase' => array(
        'remove' => 0,
        'roles' => array(
          1 => 0,
          2 => 0,
          16 => 0,
          13 => 0,
          14 => 0,
          15 => 0,
          3 => 0,
          6 => 0,
          12 => 0,
          5 => 0,
          4 => 0,
          7 => 0,
          8 => 0,
          9 => 0,
          11 => 0,
          10 => 0,
          17 => 0,
        ),
      ),
      'containing_none' => array(
        'remove' => 0,
        'roles' => array(
          1 => 0,
          2 => 0,
          16 => 0,
          13 => 0,
          14 => 0,
          15 => 0,
          3 => 0,
          6 => 0,
          12 => 0,
          5 => 0,
          4 => 0,
          7 => 0,
          8 => 0,
          9 => 0,
          11 => 0,
          10 => 0,
          17 => 0,
        ),
      ),
      'types' => array(
        'remove' => 0,
        'roles' => array(
          1 => 0,
          2 => 0,
          16 => 0,
          13 => 0,
          14 => 0,
          15 => 0,
          3 => 0,
          6 => 0,
          12 => 0,
          5 => 0,
          4 => 0,
          7 => 0,
          8 => 0,
          9 => 0,
          11 => 0,
          10 => 0,
          17 => 0,
        ),
        'filter' => array(
          'uw_site_footer' => 0,
          'uw_web_page' => 0,
        ),
        'groupings' => array(),
      ),
      'category' => array(
        'remove' => 0,
        'roles' => array(
          0 => 0,
          1 => 0,
          2 => 0,
          16 => 0,
          13 => 0,
          14 => 0,
          15 => 0,
          3 => 0,
          6 => 0,
          12 => 0,
          5 => 0,
          4 => 0,
          7 => 0,
          8 => 0,
          9 => 0,
          11 => 0,
          10 => 0,
          17 => 0,
        ),
      ),
    ),
    'restrictions' => array(
      'admin_bypass' => 1,
    ),
  );
  $export['searcher_config'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_ct_catalog_search_enable';
  $strongarm->value = '1';
  $export['uw_ct_catalog_search_enable'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_ct_catalog_wide_enable';
  $strongarm->value = '1';
  $export['uw_ct_catalog_wide_enable'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_moderation_default_state_uw_catalog';
  $strongarm->value = 'draft';
  $export['workbench_moderation_default_state_uw_catalog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'xmlsitemap_settings_node_uw_catalog';
  $strongarm->value = array(
    'status' => '1',
    'priority' => '0.5',
  );
  $export['xmlsitemap_settings_node_uw_catalog'] = $strongarm;

  return $export;
}
