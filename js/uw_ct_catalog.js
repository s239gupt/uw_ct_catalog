/**
 * @file
 *  JS for Owl-Carousel
 */

(function ($) {

    Drupal.behaviors.uw_ct_catalog = {
        attach: function (context, settings) {
            $('#catalog-tab').owlCarousel({
                items:4,
                itemsDesktop:[1e3,4],
                itemsDesktopSmall:[900,4],
                itemsTablet:[600,3],
                itemsMobile:[320,2],
                pagination:!1,
            });
        }
    };

})(jQuery);