<?php
/**
 * @file
 * uw_ct_catalog.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_catalog_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer catalog configuration'.
  $permissions['administer catalog configuration'] = array(
    'name' => 'administer catalog configuration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'uw_ct_catalog',
  );

  // Exported permission: 'create uw_catalog content'.
  $permissions['create uw_catalog content'] = array(
    'name' => 'create uw_catalog content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_catalog content'.
  $permissions['delete any uw_catalog content'] = array(
    'name' => 'delete any uw_catalog content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_catalog content'.
  $permissions['delete own uw_catalog content'] = array(
    'name' => 'delete own uw_catalog content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_catalog content'.
  $permissions['edit any uw_catalog content'] = array(
    'name' => 'edit any uw_catalog content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_catalog content'.
  $permissions['edit own uw_catalog content'] = array(
    'name' => 'edit own uw_catalog content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_catalog revision log entry'.
  $permissions['enter uw_catalog revision log entry'] = array(
    'name' => 'enter uw_catalog revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_catalog published option'.
  $permissions['override uw_catalog published option'] = array(
    'name' => 'override uw_catalog published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_catalog revision option'.
  $permissions['override uw_catalog revision option'] = array(
    'name' => 'override uw_catalog revision option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_catalog content'.
  $permissions['search uw_catalog content'] = array(
    'name' => 'search uw_catalog content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
